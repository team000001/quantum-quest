uniform sampler2D tex;
uniform float size;
uniform float N;

varying vec2 index;
varying float linearIndex;

vec4 pack_depthu(const in float toPack)
{
	float depth = toPack;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask;
	return res;
}

float unpack_depthu(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return depth;
}

void main()
{
	float k = floor(linearIndex * size / N);
	vec2 fftIndex = mod(index + vec2(k * N / size, 0.0), N);

	float He = unpack_depthu(texture2D(tex, fftIndex / N));
	float Ho = unpack_depthu(texture2D(tex, (fftIndex + vec2(N / size, 0.0)) / N));

	float sum;
	if (He > Ho)
		sum = He;
	else
		sum = Ho;
	gl_FragColor = pack_depthu(sum);
}

