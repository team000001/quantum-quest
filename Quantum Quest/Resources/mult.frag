uniform sampler2D tex;
uniform vec2 screenSize;
uniform float k;

vec4 pack_depth(const in float toPack)
{
	float depth = toPack / 2. + 0.5;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask * 1.;
	return res;
}

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

void main()
{
	gl_FragColor = pack_depth(k * unpack_depth(texture2D(tex, gl_FragCoord.xy / screenSize.x)));
}
