uniform sampler2D inR;
uniform sampler2D inI;
uniform float N;
uniform float mass;
uniform float planc;
uniform float dt;
uniform float scale;

vec4 pack_depth(const in float toPack)
{
	float depth = toPack / 2. + 0.5;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask * 1.;
	return res;
}

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

void main()
{
	vec2 pos = gl_FragCoord.xy;
	vec2 impulse = (pos - vec2(0.5) - vec2(N / 2.0)) / scale;
	float angle = dot(impulse, impulse) * dt / mass / planc / 4.0;
	vec2 psi = vec2(unpack_depth(texture2D(inR, pos / vec2(N))), 
			unpack_depth(texture2D(inI, pos / vec2(N))));

	gl_FragColor = pack_depth(dot(psi, vec2(cos(angle), -sin(angle))));
}

