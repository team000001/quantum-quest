uniform sampler2D tex;
uniform vec2 screenSize;
uniform vec2 playerSize;

void main()
{
		gl_FragColor = texture2D(tex, ((screenSize - playerSize)/2.0 + gl_FragCoord.xy ) / screenSize);
}