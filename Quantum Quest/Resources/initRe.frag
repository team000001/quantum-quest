uniform vec2 pos;
//uniform float radius;
uniform float impulse;
uniform vec2 screenSize;

vec4 pack_depth(const in float toPack)
{
	float depth = toPack / 2. + 0.5;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask;
	return res;
}

void main()
{
	float dk = impulse;
//	float posit = gl_FragCoord.x / screenSize.x - pos.x;
//	vec2 relpos = mod(gl_FragCoord.xy + screenSize / 2., screenSize) / screenSize;
	vec2 relpos = gl_FragCoord.xy / screenSize;
	float posit = length(relpos - pos);
	gl_FragColor = pack_depth(sin(-impulse * posit / 2.) / posit / impulse / 5000.0);
//	if (posit <= impulse / screenSize.x)
//		gl_FragColor = pack_depth(0.0001);
//	else
//		gl_FragColor = pack_depth(0.0);
}

