uniform sampler2D tex;
uniform vec2 screenSize;
uniform float maxH;
uniform float minH;
uniform float scale;
uniform float level;
uniform vec2 worldSize;
uniform vec2 cornerPos;

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return depth;
}

vec4 packFloatToVec4i(const float value)
{
	const vec4 bitSh = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bitMsk = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(value * bitSh);
	res -= res.xxyz * bitMsk;
	return res;
}
vec4 makeColor(const float value)
{
	return vec4(mod(value*255.0*255.0*255.0, 255.0) / 255.0,
				mod(float(int(value*255.0*255.0*255.0) / 255), 255.0) / 255.0,
				float(int(value*255.0*255.0*255.0) / 255/ 255) / 255.0, 1.0);
}

void main()
{
	float br = unpack_depth(texture2D(tex, gl_FragCoord.xy / screenSize));
	br = (br - minH) / (maxH - minH) / (1.0); //+ log(scale) / 5.0);
	vec4 colorization;
	vec2 pos = cornerPos + gl_FragCoord.xy / scale;
	if (level == 0.0)
		colorization = vec4(0.7 + 0.15 * sin(10.0 * 3.14 * (pos.x / worldSize.x + pos.y / worldSize.y)),
							0.7 + 0.15 * cos(3.0 * 3.14 * (pos.x / worldSize.x + pos.y / worldSize.y)),
							0.8 + 0.1 * (pos.x / worldSize.x +  pos.y / worldSize.y), 1.0);
	else
		colorization = vec4(1.0, 1.0, 1.0, 1.0);
	//gl_FragColor = packFloatToVec4i(br);
	gl_FragColor = vec4(br, br, br, 1.0) * colorization;
}

