uniform sampler2D playerRe;
uniform sampler2D playerIm;
uniform vec2 screenSize;
uniform float normalization;

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*256.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

vec3 hsv2rgb(vec3 c)
{
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main()
{
	vec2 psi;
//	vec2 relpos = mod(gl_FragCoord.xy + screenSize / 2., screenSize) / screenSize;
	vec2 relpos = gl_FragCoord.xy / screenSize;
	if (length(relpos - vec2(0.5)) < 0.5)
	{
		psi.x = unpack_depth(texture2D(playerRe, relpos));
		psi.y = unpack_depth(texture2D(playerIm, relpos));
		psi /= normalization / 1.7;
		float module = dot(psi, psi);
		float phase = atan(psi.y, psi.x) / 2. / 3.1415926 + 0.5;
		vec3 hsv = vec3(phase, 1., 1.);
		gl_FragColor = vec4(hsv2rgb(hsv), module);
	}
	else
		gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
//	gl_FragColor = vec4(module, module, module, 1.);
}

