uniform sampler2D inR;
uniform sampler2D inI;
uniform float N;
uniform float size;
uniform float shift;

varying vec2 index;
varying float linearIndex;

#define PI 3.1415926

vec4 pack_depth(const in float toPack)
{
	float depth = toPack / 2. + 0.5;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask * 1.;
	return res;
}

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

vec2 complex_mult(vec2 a, vec2 b)
{
	return vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

vec2 W(float X)
{
	float angle = 2.0 * PI * X / size;
	return vec2(cos(angle), sin(angle));
}

void main()
{
	float k = floor(linearIndex * size / N);
	vec2 fftIndex = mod(index + vec2(0.0, k * N / size), N);

	vec2 HeCoord = fftIndex / N;
	vec2 He = vec2(unpack_depth(texture2D(inR, HeCoord)), 
			unpack_depth(texture2D(inI, HeCoord)));

	vec2 HoCoord = (fftIndex + vec2(0.0, N / size)) / N;
	vec2 Ho = vec2(unpack_depth(texture2D(inR, HoCoord)), 
			unpack_depth(texture2D(inI, HoCoord)));

	vec2 Wn = W(k);
	vec2 WHo = complex_mult(Wn, Ho.xy);
	vec2 result = He + WHo;
	if (shift > 0.5)
		result = complex_mult(W(linearIndex * N / 2.), result);
	gl_FragColor = pack_depth(result.x);
}

