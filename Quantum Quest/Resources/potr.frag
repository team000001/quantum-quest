uniform sampler2D inR;
uniform sampler2D inI;
uniform sampler2D potential;
uniform vec2 force;
uniform vec2 shift;
uniform float blowUp;
uniform float N;
uniform float planc;
uniform float dt;

vec4 pack_depth(const in float toPack)
{
	float depth = toPack / 2. + 0.5;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask * 1.;
	return res;
}

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

bool inLimits(vec2 pos)
{
	if (pos.x > N)
		return false;
	if (pos.x < 0.0)
		return false;
	if (pos.y > N)
		return false;
	if (pos.y < 0.0)
		return false;
	return true;
}

void main()
{
//	vec2 pos = gl_FragCoord.xy / N;
	vec2 pos = (gl_FragCoord.xy + shift) / N;
	pos = (pos - vec2(0.5)) / blowUp + vec2(0.5);
	float V;
	if (inLimits(pos))
		V = unpack_depth(texture2D(potential, vec2(pos.x, 1.0-pos.y))) / 2. + 0.5;
	else
		V = 0.0;
//	float V = texture2D(potential, fract(pos + vec2(0.5))).y;
	if (length(pos - vec2(0.5)) < 0.25)
		V -= dot(pos - vec2(0.5), force);
	float angle = - V * dt / planc;
	vec2 psi;
	if (inLimits(pos))
		psi = vec2(unpack_depth(texture2D(inR, pos)), 
				unpack_depth(texture2D(inI, pos)));
	else
		psi = vec2(0.0);

	gl_FragColor = pack_depth(dot(psi, vec2(cos(angle), -sin(angle))));
//	gl_FragColor = texture2D(inR, gl_FragCoord.xy / N);
}

