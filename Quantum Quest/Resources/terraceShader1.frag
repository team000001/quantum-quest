uniform vec2 cornerPos;
uniform vec2 worldSize;
uniform float scale;

vec4 packFloatToVec4i(const float value)
{
	const vec4 bitSh = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bitMsk = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(value * bitSh);
	res -= res.xxyz * bitMsk;
	return res;
}

void main()
{
	vec2 coords = cornerPos + gl_FragCoord.xy / scale;
	vec2 centCoord = coords - worldSize / 2.0;
	if (abs(centCoord.x) < 256.0/ scale && abs(centCoord.y) < 256.0 / scale)
		gl_FragColor = packFloatToVec4i(0.0);
	else
		gl_FragColor = packFloatToVec4i(0.999);
}