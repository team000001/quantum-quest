uniform float N;

varying vec2 index;
varying float linearIndex;

void main()
{
	vec2 gridVertex = N * gl_Vertex.xy;
	linearIndex = gridVertex.x;
	index = gridVertex;
	gl_Position = vec4(2.0 * (index / N - vec2(0.5)), 0.0, 1.0);
}
