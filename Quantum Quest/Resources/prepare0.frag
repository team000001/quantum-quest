uniform sampler2D waveRe;
uniform sampler2D waveIm;
uniform vec2 screenSize;

vec4 pack_depthu(const in float toPack)
{
	float depth = toPack;
	const vec4 bit_shift = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bit_mask  = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(depth * bit_shift);
	res -= res.xxyz * bit_mask;
	return res;
}

float unpack_depth(const in vec4 rgba_depth)
{
	const vec4 bit_shift = vec4(1.0/(255.0*255.0*255.0), 1.0/(255.0*255.0), 1.0/255.0, 1.0);
	float depth = dot(rgba_depth, bit_shift);
	return 2. * (depth - 0.5);
}

void main()
{
	vec2 psi;
	psi.x = unpack_depth(texture2D(waveRe, gl_FragCoord.xy / screenSize));
	psi.y = unpack_depth(texture2D(waveIm, gl_FragCoord.xy / screenSize));
	gl_FragColor = pack_depthu(length(psi));
//	gl_FragColor = pack_depthu(1.);
}

