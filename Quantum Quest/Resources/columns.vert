uniform float N;

varying vec2 index;
varying float linearIndex;

void main()
{
	vec2 gridVertex = N * gl_Vertex.xy;
	linearIndex = gridVertex.x;
	index = vec2(gridVertex.y, gridVertex.x);
	gl_Position = vec4(2.0 * (index.xy / N - vec2(0.5)), 0.0, 1.0);
}
