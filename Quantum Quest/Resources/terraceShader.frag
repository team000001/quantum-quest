uniform vec2 cornerPos;
uniform vec2 screenSize;
uniform vec2 worldSize;
uniform float scale;
uniform float prec;
uniform float maxSize;
uniform float playfullness;
#define PI 3.14159265

float snoise(vec3 v);
vec2 Decart (float r, float fi) {
	return vec2(r * cos(fi), r * sin(fi));
}
float sumSnoise ()
{
	float res = 0.0;
	float size = maxSize;
	vec2 coords = cornerPos + gl_FragCoord.xy / scale;
	
	while (size * max(screenSize.x, screenSize.y) * scale > prec) {
		res += size * (0.5 + snoise(vec3(coords*size, 0.0)) / 2.0);
		size *= playfullness;
	}
	res *= (1.0 - playfullness);
	
	vec2 centCoord = coords - worldSize / 2.0;
	float r = length(centCoord),
	fi = atan(centCoord.y / centCoord.x);
	if (centCoord.x < 0.0)
		fi += PI;
	float mainLabyrinth;// = (0.5 + sin(r / 10.0) / 2.0);
	vec2 spiral = Decart(r, r / 25.0);
	if (length(centCoord - spiral) < 300.0) {
		mainLabyrinth = 0.65 * cos(length(centCoord - spiral) / 300.0 * PI / 2.0);
	}
	else
		mainLabyrinth = 0.0;
	mainLabyrinth += res * 0.35;
	return mainLabyrinth;
	if (r > 300.0 && r < 800.0 && fi > PI / 4.0 && fi < 3.0 * PI / 4.0)
		return (0.5 + sin(r / 10.0) / 2.0) * abs(sin(2.0 * fi));
	else
		return 0.5 + sin(r / 10.0) / 2.0;
	/*
	return 0.6 * abs(sin(( (cornerPos.y + scaledCoord.y) * (cornerPos.x + scaledCoord.x))/5000.0))
			+ 0.3 * abs(sin(length(worldSize / 2.0 - cornerPos - scaledCoord ))/300.0) + 0.1 * res;
	*/
}

vec4 packFloatToVec4i(const float value)
{
	const vec4 bitSh = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bitMsk = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(value * bitSh);
	res -= res.xxyz * bitMsk;
	return res;
}

void main()
{
	gl_FragColor = packFloatToVec4i(sumSnoise());
}



// outer code vvvvvvvvvvvvvvvvvvvvvvvvvvvvvv



vec4 permute( vec4 x ) {
	
	return mod( ( ( x * 34.0 ) + 1.0 ) * x, 289.0 );
	
}

vec4 taylorInvSqrt( vec4 r ) {
	
	return 1.79284291400159 - 0.85373472095314 * r;
	
}

float snoise( vec3 v ) {
	
	const vec2 C = vec2( 1.0 / 6.0, 1.0 / 3.0 );
	const vec4 D = vec4( 0.0, 0.5, 1.0, 2.0 );
	
	// First corner
	
	vec3 i  = floor( v + dot( v, C.yyy ) );
	vec3 x0 = v - i + dot( i, C.xxx );
	
	// Other corners
	
	vec3 g = step( x0.yzx, x0.xyz );
	vec3 l = 1.0 - g;
	vec3 i1 = min( g.xyz, l.zxy );
	vec3 i2 = max( g.xyz, l.zxy );
	
	vec3 x1 = x0 - i1 + 1.0 * C.xxx;
	vec3 x2 = x0 - i2 + 2.0 * C.xxx;
	vec3 x3 = x0 - 1. + 3.0 * C.xxx;
	
	// Permutations
	
	i = mod( i, 289.0 );
	vec4 p = permute( permute( permute(
									   i.z + vec4( 0.0, i1.z, i2.z, 1.0 ) )
							  + i.y + vec4( 0.0, i1.y, i2.y, 1.0 ) )
					 + i.x + vec4( 0.0, i1.x, i2.x, 1.0 ) );
	
	// Gradients
	// ( N*N points uniformly over a square, mapped onto an octahedron.)
	
	float n_ = 1.0 / 7.0; // N=7
	
	vec3 ns = n_ * D.wyz - D.xzx;
	
	vec4 j = p - 49.0 * floor( p * ns.z *ns.z );  //  mod(p,N*N)
	
	vec4 x_ = floor( j * ns.z );
	vec4 y_ = floor( j - 7.0 * x_ );    // mod(j,N)
	
	vec4 x = x_ *ns.x + ns.yyyy;
	vec4 y = y_ *ns.x + ns.yyyy;
	vec4 h = 1.0 - abs( x ) - abs( y );
	
	vec4 b0 = vec4( x.xy, y.xy );
	vec4 b1 = vec4( x.zw, y.zw );
	
	
	vec4 s0 = floor( b0 ) * 2.0 + 1.0;
	vec4 s1 = floor( b1 ) * 2.0 + 1.0;
	vec4 sh = -step( h, vec4( 0.0 ) );
	
	vec4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
	vec4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
	
	vec3 p0 = vec3( a0.xy, h.x );
	vec3 p1 = vec3( a0.zw, h.y );
	vec3 p2 = vec3( a1.xy, h.z );
	vec3 p3 = vec3( a1.zw, h.w );
	
	// Normalise gradients
	
	vec4 norm = taylorInvSqrt( vec4( dot( p0, p0 ), dot( p1, p1 ), dot( p2, p2 ), dot( p3, p3 ) ) );
	p0 *= norm.x;
	p1 *= norm.y;
	p2 *= norm.z;
	p3 *= norm.w;
	
	// Mix final noise value
	
	vec4 m = max( 0.6 - vec4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
	m = m * m;
	return 42.0 * dot( m*m, vec4( dot( p0, x0 ), dot( p1, x1 ),
								 dot( p2, x2 ), dot( p3, x3 ) ) );
	
}
/*
void main()
{
	//extreme version
	float result = 0.0, maxH = 1.0, del = 1.0, i, j;
	vec2 pos = gl_FragCoord.xy, square, peakPos, squareSize = WORLD_SIZE;
	for (i = 0.0; max(squareSize.x, squareSize.y) > 50.0 / scale; i++) {
		for (j = 0.0; j < del * del; j++) {
			square = vec2(int(pos.x / squareSize.x), int(pos.y / squareSize.y));
			peakPos = (square + vec2 (0.5, 0.5)) / del;
			peakPos.x *= WORLD_SIZE.x;
			peakPos.y *= WORLD_SIZE.y;
			if (length(pos - peakPos) < min(squareSize.x, squareSize.y) / 2.0)
				result += (0.5 - fract(sin(dot(peakPos.xy, vec2(12.9898, 78.233)))* 43758.5453))
						* maxH * cos(length(pos - peakPos) / (WORLD_SIZE.x / del / 2.0));
			maxH *= playfullness;
		}
		squareSize /= 2.0;
		del *= 2.0;
	}
 
	float br = 0.5 + 0.5 * (1.0 - playfullness) * result;
	vec3 colors = vec3(br, br, br);
	gl_FragColor = vec4(colors, 1.0);
}
*/