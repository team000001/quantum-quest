uniform vec2 cornerPos;
uniform vec2 worldSize;
uniform float scale;

vec4 packFloatToVec4i(const float value)
{
	const vec4 bitSh = vec4(255.0*255.0*255.0, 255.0*255.0, 255.0, 1.0);
	const vec4 bitMsk = vec4(0.0, 1.0/255.0, 1.0/255.0, 1.0/255.0);
	vec4 res = fract(value * bitSh);
	res -= res.xxyz * bitMsk;
	return res;
}

float pot () {
	vec2 coords = cornerPos + gl_FragCoord.xy / scale;
	vec2 centCoord = coords - worldSize / 2.0;
	float r = length(centCoord),
	fi = atan(centCoord.y / centCoord.x);
	if (r < 256.0/scale)
		return 1.0-(1.0-exp(-r/256.0*scale+10.0/scale))*(1.0-exp(-r/256.0*scale+10.0/scale));
	else
		return 0.999;
}

void main()
{
	gl_FragColor = packFloatToVec4i(pot());
}