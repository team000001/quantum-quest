let root = getcwd()
let g:ycm_seed_identifiers_with_syntax = 1 
execute 'set makeprg=cd\ '.substitute(root, " ", '\\\\\\ ', "g").'/build\ &&\ cmake\ ..\ &&\ make'
nnoremap <F4> :make!<cr>
execute "nnoremap <F5> :!".substitute(root, " ", '\\\ ', "g")."/build/quantum_quest<cr>"
execute "nnoremap <F6> :!cp\ ".substitute(root, " ", '\\\ ', "g")."/Resources/*\ ".substitute(root, " ", '\\\ ', "g")."/build/Resources/<cr>"
nnoremap <F3> :YcmDiags<cr>


