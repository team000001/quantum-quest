//-------------------------------------------------------
// Uses CoreFoundation on Mac OS to return the proper
// resource path for bundled applications
//
// Based on resourcePath.mm provided with the Xcode
// templates for SFML. This version contains no
// Objective-C code, allowing it to be compiled in
// non-Xcode IDEs so the file can be used regardless of
// platform or development environment.
//
//-------------------------------------------------------

#include "ResourcePath.h"
#include <iostream>

std::string resourcePath()
{
#ifdef linux
	char buff[1024];
	ssize_t len = readlink("/proc/self/exe", buff, sizeof(buff)-1);
	buff[len] = '\0';
	std::string var = std::string(buff);
	var.erase(var.find_last_of("/"));
	var += std::string("/Resources/");
	return var;
#else
	return "Resources//";
#endif
}

