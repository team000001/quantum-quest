//
//  projector.h
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_projector_h
#define Quantum_Quest_projector_h

#include "general.h"

class Projector {
public:
	Projector(int lev);
	void setParams(sf::Vector2f newPos, float newScale);
	void Render(sf::RenderWindow &wnd, const sf::Texture &tex,
				sf::Vector2f texCoords[4], sf::Vector2f objCoords,
				sf::Vector2f objSize);
	sf::Vector2f pos, prevPos;
	float scale, prevScale;
	int level;
};
#endif
