#include "general.h"
#include "object.h"
#include "projector.h"
#include "static.h"
#include "terrace.h"
#include "player.h"
#include "obstacles.h"
#include <iostream>

bool inBounds (sf::Vector2f vec) {
		return vec.x >= SCREEN_SIZE.x * PROJECTOR_POSITION.x
				&& vec.y >= SCREEN_SIZE.y * PROJECTOR_POSITION.y
				&& vec.x <= WORLD_SIZE.x - SCREEN_SIZE.x * (1 - PROJECTOR_POSITION.x)
				&& vec.y <= WORLD_SIZE.y - SCREEN_SIZE.y * (1 - PROJECTOR_POSITION.y);
}

float getFPS(const sf::Time& time) {
	return (1000000.0f / time.asMicroseconds());
}
int level = 0;
int startRoutine()
{
	sf::RenderWindow window(sf::VideoMode(SCREEN_SIZE.x, SCREEN_SIZE.y), GAME_TITLE);

	// Set the Icon
	sf::Image icon;
	if (!icon.loadFromFile(resourcePath() + "icon.png"))
		return error(0, "icon.png");
	window.setIcon(icon.getSize().x, icon.getSize().y, icon.getPixelsPtr());
	
	std::vector<Object *> objects;
	Terrace *terr = new Terrace();
	Player *player = new Player(terr, 1638.4, 1.0);
	player->impulses = false;
	player->potentialP = true;
	player->kineticP = true;
	player->initState(sf::Vector2f(0.5f, 0.5f), 0, 100);//36);
	objects.push_back(terr);
	//objects.push_back(new Obstacles());
	objects.push_back(player);
	
	sf::Clock clock;
	sf::Clock FPSClock;
	srand(time(NULL));
	float dt = 1e-1f, fps = 1.0f/40.0f,
		time = clock.getElapsedTime().asSeconds();
	Projector *proj = new Projector(level);
	
	bool drawing = true;
	terr->Update(dt, proj);

	while (window.isOpen()) {
		if (clock.getElapsedTime().asSeconds() - time > fps) {
			time = clock.getElapsedTime().asSeconds();
			// Process events
			sf::Event event;
			while (window.pollEvent(event)) {
				// Close window : exit
				if (event.type == sf::Event::Closed) {
					window.close();
				}
				// Escape pressed : exit
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num0) {
					level = 0;
					return 1;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num1) {
					level = 1;
					return 1;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num2) {
					level = 2;
					return 1;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num3) {
					level = 3;
					return 1;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Num4) {
					level = 4;
					return 1;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space) {
					drawing = !drawing;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Z) {
					player->potentialP = !player->potentialP;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::X) {
					player->kineticP = !player->kineticP;
				}
				if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::C) {
					if (player->impulses)
						player->Fourier();
					else
						player->FourierInv();
					player->impulses = !player->impulses;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
					player->force = sf::Vector2f(-1.0f, 0.0f);
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
					player->force = sf::Vector2f(1.0f, 0.0f);
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
					player->force = sf::Vector2f(0.0f, 1.0f);
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
					player->force = sf::Vector2f(0.0f, -1.0f);
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
					proj->scale *= 1.02f;
				}
				if(sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
					if (proj->scale * 0.98f >= 1.0f)
						proj->scale *= 0.98f;
					else
						proj->scale = 1;
				}
			}
			if (drawing)
			{
				window.clear();
				for(int objIndex = 0; objIndex < int(objects.size()); objIndex++) {
					objects[objIndex]->Update(dt, proj);
					objects[objIndex]->Draw(window, proj);
				}
				window.display();
			}
			std::cout <<1000.0f/getFPS(FPSClock.restart()) << std::endl;
		}
	}
	for(int objIndex = 0; objIndex < int(objects.size()); objIndex++) {
		delete objects[objIndex];
	}
	return EXIT_SUCCESS;
}

int main () {
	while (startRoutine());
	return EXIT_SUCCESS;
}
