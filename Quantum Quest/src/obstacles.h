//
//  obstacles.h
//  Quantum Quest
//
//  Created by Денис Дёмин on 19.05.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_obstacle_h
#define Quantum_Quest_obstacle_h

#include "static.h"
#include "object.h"
#include "projector.h"
#include "static.h"

class Obstacles: public Object {
public:
	Obstacles();
	~Obstacles();
	virtual void Update(float dt, Projector *proj);
	virtual float GetDepth();
	virtual void Draw(sf::RenderWindow &wnd, Projector *proj);
	std::vector<Static *> arr;
	std::vector<sf::Vector2f *> absPosArr;
private:
	std::vector<sf::String> texNames;
	std::vector<sf::Vector2i> destroyed;
};


#endif
