//
//  projector.cpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#include "projector.h"

Projector::Projector(int lev)
{
	pos = sf::Vector2f(WORLD_SIZE.x / 2,
					   WORLD_SIZE.y / 2);
	scale = 3;
	prevPos = pos;
	prevScale = scale + 1e-6f;
	level = lev;
}

void Projector::setParams(sf::Vector2f newPos, float newScale)
{
	prevPos = pos;
	prevScale = scale;
	pos += newPos;
	scale *= newScale;
}

void Projector::Render(sf::RenderWindow &wnd, const sf::Texture &tex,
			sf::Vector2f texCoords[4], sf::Vector2f objCoords,
			sf::Vector2f objSize)
{
	sf::Vertex vertices[4];
	for (int i = 0; i < 4; i++) {
		vertices[i].texCoords = texCoords[i];
		vertices[i].position = objCoords;
	}
	objSize *= 0.5f;
	vertices[0].position.x += - objSize.x;
	vertices[0].position.y += - objSize.y;
	vertices[1].position.x += + objSize.x;
	vertices[1].position.y += - objSize.y;
	vertices[2].position.x += + objSize.x;
	vertices[2].position.y += + objSize.y;
	vertices[3].position.x += - objSize.x;
	vertices[3].position.y += + objSize.y;
	wnd.draw(vertices, 4, sf::Quads, &tex);
}
