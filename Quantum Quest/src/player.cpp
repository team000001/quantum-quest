#include "player.h"
#include <iostream>

void Player::getScreen(sf::Vertex vertices[4], sf::Vector2f size)
{
	vertices[0].position.x = 0.0f;
	vertices[0].position.y = 0.0f;
	vertices[0].texCoords = vertices[0].position;
	vertices[1].position.x = size.x;
	vertices[1].position.y = 0.0f;
	vertices[1].texCoords = vertices[1].position;
	vertices[2].position.x = size.x;
	vertices[2].position.y = size.y;
	vertices[2].texCoords = vertices[2].position;
	vertices[3].position.x = 0.0f;
	vertices[3].position.y = size.y;
	vertices[3].texCoords = vertices[3].position;
}

Player::Player(Terrace *terr, float mass, float planc)
{
	oldRe = new sf::RenderTexture();
	oldRe->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	oldRe->setSmooth(true);

	nextRe = new sf::RenderTexture();
	nextRe->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	nextRe->setSmooth(true);

	oldIm = new sf::RenderTexture();
	oldIm->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	oldIm->setSmooth(true);

	nextIm = new sf::RenderTexture();
	nextIm->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	nextIm->setSmooth(true);
	
	drawOldRe = new sf::RenderTexture();
	drawOldRe->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	drawOldRe->setSmooth(true);

	drawNextRe = new sf::RenderTexture();
	drawNextRe->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	drawNextRe->setSmooth(true);

	drawOldIm = new sf::RenderTexture();
	drawOldIm->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	drawOldIm->setSmooth(true);

	drawNextIm = new sf::RenderTexture();
	drawNextIm->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	drawNextIm->setSmooth(true);

	old = new sf::RenderTexture;
	old->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	old->setSmooth(true);

	next = new sf::RenderTexture;
	next->create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	next->setSmooth(true);

	drawTexture.create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	drawTexture.setSmooth(true);

	initShaderRe.loadFromFile(resourcePath()+"initRe.frag", sf::Shader::Fragment);
	initShaderIm.loadFromFile(resourcePath()+"initIm.frag", sf::Shader::Fragment);
	drawShader.loadFromFile(resourcePath()+"setcolorReIm.frag", sf::Shader::Fragment);
	summx.loadFromFile(resourcePath()+"lines.vert", resourcePath()+"summx.frag");
	maxShx.loadFromFile(resourcePath()+"lines.vert", resourcePath()+"maxx.frag");
	summy.loadFromFile(resourcePath()+"columns.vert", resourcePath()+"summy.frag");
	maxShy.loadFromFile(resourcePath()+"columns.vert", resourcePath()+"maxy.frag");
	prepare0.loadFromFile(resourcePath()+"prepare0.frag", sf::Shader::Fragment);
	prepare1.loadFromFile(resourcePath()+"prepare1.frag", sf::Shader::Fragment);
	prepare2.loadFromFile(resourcePath()+"prepare2.frag", sf::Shader::Fragment);
	prepare3.loadFromFile(resourcePath()+"prepare3.frag", sf::Shader::Fragment);
	prepare4.loadFromFile(resourcePath()+"prepare4.frag", sf::Shader::Fragment);

	fftxr.loadFromFile(resourcePath()+"lines.vert", resourcePath()+"fftR1.frag");
	fftxi.loadFromFile(resourcePath()+"lines.vert", resourcePath()+"fftI1.frag");
	fftyr.loadFromFile(resourcePath()+"columns.vert", resourcePath()+"fftR2.frag");
	fftyi.loadFromFile(resourcePath()+"columns.vert", resourcePath()+"fftI2.frag");
	inverse.loadFromFile(resourcePath()+"mult.frag", sf::Shader::Fragment);

	kinShr.loadFromFile(resourcePath()+"kinr.frag", sf::Shader::Fragment);
	kinShi.loadFromFile(resourcePath()+"kini.frag", sf::Shader::Fragment);
	potShr.loadFromFile(resourcePath()+"potr.frag", sf::Shader::Fragment);
	potShi.loadFromFile(resourcePath()+"poti.frag", sf::Shader::Fragment);

	this->terr = terr;
	this->mass = mass;
	this->planc = planc;
	fftxr.setParameter("N", PLAYER_SIZE.x);
	fftxi.setParameter("N", PLAYER_SIZE.x);
	fftyr.setParameter("N", PLAYER_SIZE.x);
	fftyi.setParameter("N", PLAYER_SIZE.x);
	kinShr.setParameter("N", PLAYER_SIZE.x);
	kinShi.setParameter("N", PLAYER_SIZE.x);
	potShr.setParameter("N", PLAYER_SIZE.x);
	potShi.setParameter("N", PLAYER_SIZE.x);
	summx.setParameter("N", PLAYER_SIZE.x);
	summy.setParameter("N", PLAYER_SIZE.x);
	maxShx.setParameter("N", PLAYER_SIZE.x);
	maxShy.setParameter("N", PLAYER_SIZE.x);
	drawShader.setParameter("screenSize", PLAYER_SIZE);
	inverse.setParameter("screenSize", PLAYER_SIZE);
	prepare0.setParameter("screenSize", PLAYER_SIZE);
	prepare1.setParameter("screenSize", PLAYER_SIZE);
	prepare2.setParameter("screenSize", PLAYER_SIZE);
	prepare3.setParameter("screenSize", PLAYER_SIZE);
	prepare4.setParameter("screenSize", PLAYER_SIZE);

	kinShr.setParameter("planc", planc);
	kinShi.setParameter("planc", planc);
	potShr.setParameter("planc", planc);
	potShi.setParameter("planc", planc);
	kinShr.setParameter("mass", mass);
	kinShi.setParameter("mass", mass);

	normalization = 1.;
	compress = PLAYER_DRAW_SIZE.x / PLAYER_SIZE.x;
	force = sf::Vector2f(0.0f, 0.0f);
}

Player::~Player()
{
	delete oldRe;
	delete nextRe;
	delete oldIm;
	delete nextIm;
	delete old;
	delete next;
}

float Player::GetDepth()
{
	return -1e10f;
}

void Player::Update(float dt, Projector *proj)
{
	if (kineticP)
		Impulse(dt, proj->prevScale / compress);
	Fourier();
	if (potentialP)
		Potential(dt, proj);
	FourierInv();
	if (kineticP)
		Impulse(dt, proj->scale / compress);

	sf::Vertex vertices[4];
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;
	getScreen(vertices, PLAYER_SIZE);
	state.texture = &oldRe->getTexture();
	drawOldRe->draw(vertices, 4, sf::Quads, state);
	state.texture = &oldIm->getTexture();
	drawOldIm->draw(vertices, 4, sf::Quads, state);
	drawOldRe->display();
	drawOldIm->display();

	if (!impulses)
		fft(drawOldRe, drawOldIm, drawNextIm, drawNextRe);

	normalization = summate(&prepare0, &maxShx, &maxShy);
	float summres = summate(&prepare1, &summx, &summy);
	float summresx = summate(&prepare2, &summx, &summy);
	float summresy = summate(&prepare3, &summx, &summy);
	
	sf::Vector2f newPos = sf::Vector2f(summresx / summres, summresy / summres);
//	prepare4.setParameter("center", newPos);
//	float sigma = summate(&prepare4, &summx, &summy);
	float k = 1.0;
	newPos -= PROJECTOR_POSITION;
	if (sqrt(newPos.x*newPos.x + newPos.y*newPos.y) < 0.01f)
		newPos = sf::Vector2f(0.0f, 0.0f);
	proj->setParams(newPos * PLAYER_DRAW_SIZE.x / proj->scale, k);//1.5e-7f / sigma);
}

void Player::Draw(sf::RenderWindow &wnd, Projector *proj)
{
	sf::Vertex vertices[4];
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;
	getScreen(vertices, PLAYER_SIZE);

	drawShader.setParameter("playerRe", drawOldRe->getTexture());
	drawShader.setParameter("playerIm", drawOldIm->getTexture());
	drawShader.setParameter("normalization", normalization);
	state.shader = &drawShader;

	drawTexture.draw(vertices, 4, sf::Quads, state);
	drawTexture.display();

	sf::Vector2f texCoords[4];
	texCoords[0].x = 0.0f;
	texCoords[0].y = 0.0f;
	texCoords[1].x = PLAYER_SIZE.x;
	texCoords[1].y = 0.0f;
	texCoords[2].x = PLAYER_SIZE.x;
	texCoords[2].y = PLAYER_SIZE.y;
	texCoords[3].x = 0.0f;
	texCoords[3].y = PLAYER_SIZE.y;
	proj->Render(wnd, drawTexture.getTexture(), texCoords, SCREEN_SIZE / 2.0f, PLAYER_DRAW_SIZE);
}

void Player::initState(sf::Vector2f pos, float radius, float impulse)
{
	oldPos = pos;
	oldDiff = 3. * radius;
	initShaderRe.setParameter("impulse", impulse);
	initShaderRe.setParameter("pos", pos);
	initShaderRe.setParameter("screenSize", PLAYER_SIZE);

	sf::Vertex vertices[4];
	getScreen(vertices, PLAYER_SIZE);

	sf::RenderStates state;
	state.blendMode = sf::BlendNone;
	state.shader = &initShaderRe;
	oldRe->draw(vertices, 4, sf::Quads, state);
	oldRe->display();
	state.shader = &initShaderIm;
	oldIm->draw(vertices, 4, sf::Quads, state);
	oldIm->display();
	FourierInv();
}

void Player::Fourier()
{
	fft(oldRe, oldIm, nextIm, nextRe);
}

void Player::FourierInv()
{
	fft(oldRe, oldIm, nextIm, nextRe);
	fft(oldRe, oldIm, nextIm, nextRe);
	fft(oldRe, oldIm, nextIm, nextRe);
}

void Player::fft(sf::RenderTexture *oldRe,
			sf::RenderTexture *oldIm,
			sf::RenderTexture *nextIm,
			sf::RenderTexture *nextRe)
{
	sf::RenderTexture *tmp;
	float partSize;
	sf::Vertex vertices[4];
	getScreen(vertices, PLAYER_SIZE);
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;

	fftxr.setParameter("shift", 0.0);
	fftxi.setParameter("shift", 0.0);
	partSize = 1.0f;
	for (int i = 1; i <= PLAYER_POWER; i++)
	{
		partSize *= 2.0f;
		fftxr.setParameter("inR", oldRe->getTexture());
		fftxr.setParameter("inI", oldIm->getTexture());
		fftxr.setParameter("size", partSize);
		fftxi.setParameter("inR", oldRe->getTexture());
		fftxi.setParameter("inI", oldIm->getTexture());
		fftxi.setParameter("size", partSize);
		if (i == PLAYER_POWER)
		{
			fftxr.setParameter("shift", 1.0);
			fftxi.setParameter("shift", 1.0);
		}

		state.shader = &fftxr;
		nextRe->draw(vertices, 4, sf::Quads, state);
		state.shader = &fftxi;
		nextIm->draw(vertices, 4, sf::Quads, state);

		nextRe->display();
		nextIm->display();
		tmp = oldRe;
		oldRe = nextRe;
		nextRe = tmp;
		tmp = oldIm;
		oldIm = nextIm;
		nextIm = tmp;
	}

	inverse.setParameter("tex", oldIm->getTexture());
	inverse.setParameter("k", 1.0f / sqrt(PLAYER_SIZE.x));
	state.shader = &inverse;
	nextIm->draw(vertices, 4, sf::Quads, state);
	inverse.setParameter("tex", oldRe->getTexture());
	inverse.setParameter("k", 1.0f / sqrt(PLAYER_SIZE.x));
	state.shader = &inverse;
	nextRe->draw(vertices, 4, sf::Quads, state);
	nextIm->display();
	nextRe->display();
	tmp = oldRe;
	oldRe = nextRe;
	nextRe = tmp;
	tmp = oldIm;
	oldIm = nextIm;
	nextIm = tmp;

	fftyr.setParameter("shift", 0.0);
	fftyi.setParameter("shift", 0.0);
	partSize = 1.0f;
	for (int i = 1; i <= PLAYER_POWER; i++)
	{
		partSize *= 2.0f;
		fftyr.setParameter("inR", oldRe->getTexture());
		fftyr.setParameter("inI", oldIm->getTexture());
		fftyr.setParameter("size", partSize);
		fftyi.setParameter("inR", oldRe->getTexture());
		fftyi.setParameter("inI", oldIm->getTexture());
		fftyi.setParameter("size", partSize);
		if (i == PLAYER_POWER)
		{
			fftyr.setParameter("shift", 1.0);
			fftyi.setParameter("shift", 1.0);
		}

		state.shader = &fftyr;
		nextRe->draw(vertices, 4, sf::Quads, state);
		state.shader = &fftyi;
		nextIm->draw(vertices, 4, sf::Quads, state);

		nextRe->display();
		nextIm->display();
		tmp = oldRe;
		oldRe = nextRe;
		nextRe = tmp;
		tmp = oldIm;
		oldIm = nextIm;
		nextIm = tmp;
	}

	inverse.setParameter("tex", oldIm->getTexture());
	inverse.setParameter("k", -1.0f / sqrt(PLAYER_SIZE.x));
	state.shader = &inverse;
	nextIm->draw(vertices, 4, sf::Quads, state);
	inverse.setParameter("tex", oldRe->getTexture());
	inverse.setParameter("k", 1.0f / sqrt(PLAYER_SIZE.x));
	state.shader = &inverse;
	nextRe->draw(vertices, 4, sf::Quads, state);
	nextIm->display();
	nextRe->display();
	tmp = oldRe;
	oldRe = nextRe;
	nextRe = tmp;
	tmp = oldIm;
	oldIm = nextIm;
	nextIm = tmp;
}

void Player::Impulse(float dt, float scale)
{
	sf::RenderTexture *tmp;
	sf::Vertex vertices[4];
	getScreen(vertices, PLAYER_SIZE);
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;
	kinShr.setParameter("dt", dt);
	kinShr.setParameter("scale", scale);
	kinShr.setParameter("inR", oldRe->getTexture());
	kinShr.setParameter("inI", oldIm->getTexture());
	kinShi.setParameter("dt", dt);
	kinShi.setParameter("scale", scale);
	kinShi.setParameter("inR", oldRe->getTexture());
	kinShi.setParameter("inI", oldIm->getTexture());
	state.shader = &kinShr;
	nextRe->draw(vertices, 4, sf::Quads, state);
	state.shader = &kinShi;
	nextIm->draw(vertices, 4, sf::Quads, state);
	nextRe->display();
	nextIm->display();
	tmp = oldRe;
	oldRe = nextRe;
	nextRe = tmp;
	tmp = oldIm;
	oldIm = nextIm;
	nextIm = tmp;
}

void Player::Potential(float dt, Projector *proj)
{
	sf::RenderTexture *tmp;
	sf::Vertex vertices[4];
	getScreen(vertices, PLAYER_SIZE);
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;
	potShr.setParameter("dt", dt);
	potShr.setParameter("inR", oldRe->getTexture());
	potShr.setParameter("inI", oldIm->getTexture());
	potShr.setParameter("potential", terr->playerTex);
	potShr.setParameter("force", force);
	potShr.setParameter("shift", (proj->pos - proj->prevPos) * proj->prevScale / compress);
	potShr.setParameter("blowUp", proj->scale / proj->prevScale);
	state.shader = &potShr;
	nextRe->draw(vertices, 4, sf::Quads, state);
	potShi.setParameter("dt", dt);
	potShi.setParameter("inR", oldRe->getTexture());
	potShi.setParameter("inI", oldIm->getTexture());
	potShi.setParameter("potential", terr->tex);
	potShi.setParameter("force", force);
	potShi.setParameter("shift", (proj->pos - proj->prevPos) * proj->prevScale / compress);
	potShi.setParameter("blowUp", proj->scale / proj->prevScale);
	state.shader = &potShi;
	nextIm->draw(vertices, 4, sf::Quads, state);
	nextRe->display();
	nextIm->display();
	tmp = oldRe;
	oldRe = nextRe;
	nextRe = tmp;
	tmp = oldIm;
	oldIm = nextIm;
	nextIm = tmp;
}

float Player::summate(sf::Shader *prepare, sf::Shader *summatorx, sf::Shader *summatory)
{
	float partSize;
	sf::Vertex vertices[4];
	sf::RenderTexture *tmp;
	sf::RenderStates state;
	state.blendMode = sf::BlendNone;

	prepare->setParameter("waveRe", drawOldRe->getTexture());
	prepare->setParameter("waveIm", drawOldIm->getTexture());

	getScreen(vertices, PLAYER_SIZE);
	state.shader = prepare;
	old->draw(vertices, 4, sf::Quads, state);
	old->display();

	partSize = 1.0f;
	for (int i = 1; i <= PLAYER_POWER; i++)
	{
		partSize *= 2.0f;
		summatorx->setParameter("tex", old->getTexture());
		summatorx->setParameter("size", partSize);
		state.shader = summatorx;
		next->draw(vertices, 4, sf::Quads, state);
		next->display();
		tmp = old;
		old = next;
		next = tmp;
	}

	partSize = 1.0f;
	for (int i = 1; i <= PLAYER_POWER; i++)
	{
		partSize *= 2.0f;
		summatory->setParameter("tex", old->getTexture());
		summatory->setParameter("size", partSize);
		state.shader = summatory;
		next->draw(vertices, 4, sf::Quads, state);
		next->display();
		tmp = old;
		old = next;
		next = tmp;
	}

	sf::Color result = old->getTexture().copyToImage().getPixel(0, 0);

	return (float(result.r) / 255.f / 255.f / 255.f / 255.f +
			float(result.g) / 255.f / 255.f / 255.f +
			float(result.b) / 255.f / 255.f +
			float(result.a / 255.f));
}

