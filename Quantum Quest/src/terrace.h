//
//  terrace.h
//  Quantum Quest
//
//  Created by Денис Дёмин on 03.05.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_terrace_h
#define Quantum_Quest_terrace_h

#include "general.h"
#include "object.h"
#include "projector.h"

class Terrace : public Object {
public:
	Terrace();
	~Terrace();
	virtual void Update(float dt, Projector *proj);
	virtual float GetDepth();
	virtual void Draw(sf::RenderWindow &wnd, Projector *proj);
	sf::Texture tex;
	sf::Texture playerTex;
private:
	sf::Shader shader, shader1, shader2, shader3, shader4;
	sf::Shader cut;
	sf::Shader normalization;
	sf::RenderTexture rendTex, plTex, finalTex;
};
#endif
