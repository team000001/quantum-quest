//
//  object.hpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_object_h
#define Quantum_Quest_object_h

#include "general.h"
#include "projector.h"

class Object {
public:
	virtual ~Object(){}
	virtual void Draw(sf::RenderWindow &wnd, Projector *proj) = 0;
	virtual void Update(float dt, Projector *proj) = 0;
	virtual float GetDepth() = 0;
};

#endif
