//
//  error.cpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#include "general.h"
#include <iostream>

int error(int type, std::string message)
{
	std::cout<< "Error: " << message << std::endl;
	return EXIT_FAILURE;
}