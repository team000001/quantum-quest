//
//  general.hpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_system_h
#define Quantum_Quest_system_h

#include <math.h>
#include <climits>

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/OpenGL.hpp>

// Here is a small helper for you ! Have a look.
#include "ResourcePath.h"
#include "constants.h"
#include "time.h"

int error(int type, std::string message);

#endif
