//
//  terrace.cpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 03.05.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#include "terrace.h"
#include <iostream>

inline float colorToFloat (sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a)
{
	return float(float(r) + float(g) * 255 + float(b) * 255 * 255 + float(a) * 255 * 255 * 255)
			/ 255 / 255 / 255 / 255;
}

inline int random (int max, int min)
{
	return min + int((max - min) * float(rand()) / RAND_MAX);
}

void makeVertices (sf::Vertex *vertices, sf::Vector2f size)
{
	vertices[0].position.x = 0.0f;
	vertices[0].position.y = 0.0f;
	vertices[1].position.x = size.x;
	vertices[1].position.y = 0.0f;
	vertices[2].position.x = size.x;
	vertices[2].position.y = size.y;
	vertices[3].position.x = 0.0f;
	vertices[3].position.y = size.y;
}

Terrace::Terrace()
{
	/*
	tex.create(SCREEN_SIZE.x, SCREEN_SIZE.y);
	playerTex.create(PLAYER_SIZE.x, PLAYER_SIZE.y);
	tex.setSmooth(true);
	tex.setRepeated(true);
	*/
	if(!shader.loadFromFile(resourcePath() + "terraceShader.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceShader.frag");
	shader.setParameter("screenSize", SCREEN_SIZE);
	shader.setParameter("worldSize", WORLD_SIZE);
	shader.setParameter("prec", TERRACE_PRECISION);
	shader.setParameter("maxSize", TERRACE_MAX_SIZE);
	shader.setParameter("playfullness", TERRACE_PLAYFULLNESS);
	
	if(!shader1.loadFromFile(resourcePath() + "terraceShader1.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceShader1.frag");
	shader1.setParameter("worldSize", WORLD_SIZE);
	
	if(!shader2.loadFromFile(resourcePath() + "terraceShader2.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceShader2.frag");
	shader2.setParameter("worldSize", WORLD_SIZE);
	
	if(!shader3.loadFromFile(resourcePath() + "terraceShader3.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceShader3.frag");
	shader3.setParameter("worldSize", WORLD_SIZE);
	
	if(!shader4.loadFromFile(resourcePath() + "terraceShader4.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceShader4.frag");
	shader4.setParameter("worldSize", WORLD_SIZE);
	
	if(!cut.loadFromFile(resourcePath() + "terraceCut.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceCut.frag");
	cut.setParameter("screenSize", SCREEN_SIZE);
	cut.setParameter("playerSize", PLAYER_DRAW_SIZE);
	
	if(!normalization.loadFromFile(resourcePath() + "terraceNormalize.frag", sf::Shader::Fragment))
		error(0, "Shader loading failed terraceNormalize.frag");
	normalization.setParameter("screenSize", SCREEN_SIZE);
	normalization.setParameter("worldSize", WORLD_SIZE);
	
	rendTex.create(SCREEN_SIZE.x, SCREEN_SIZE.y);
	plTex.create(PLAYER_DRAW_SIZE.x, PLAYER_DRAW_SIZE.y);
	playerTex = plTex.getTexture();
	finalTex.create(SCREEN_SIZE.x, SCREEN_SIZE.y);
	tex = finalTex.getTexture();
}
Terrace::~Terrace(){}

void Terrace::Update(float dt, Projector *proj) {
	if (proj->pos == proj->prevPos && proj->scale == proj->prevScale)
		return;
	sf::Vector2f relProjPos = sf::Vector2f(PROJECTOR_POSITION.x * SCREEN_SIZE.x,
										   PROJECTOR_POSITION.y * SCREEN_SIZE.y);
	sf::Vector2f cornerPos = proj->pos - relProjPos / proj->scale;
	
	shader.setParameter("scale", proj->scale);
	shader.setParameter("cornerPos", cornerPos);
	
	sf::Vertex vertices[4];
	sf::RenderStates state;
	
	state.blendMode = sf::BlendNone;
	switch (proj->level) {
		case 0:
			state.shader = &shader;
		break;
		case 1:
			shader1.setParameter("scale", proj->scale);
			shader1.setParameter("cornerPos", cornerPos);
			state.shader = &shader1;
		break;
		case 2:
			shader2.setParameter("scale", proj->scale);
			shader2.setParameter("cornerPos", cornerPos);
			state.shader = &shader2;
		break;
		case 3:
			shader3.setParameter("scale", proj->scale);
			shader3.setParameter("cornerPos", cornerPos);
			state.shader = &shader3;
		break;
		case 4:
			shader4.setParameter("scale", proj->scale);
			shader4.setParameter("cornerPos", cornerPos);
			state.shader = &shader4;
		break;
	}
		
	
	makeVertices(vertices, SCREEN_SIZE);
	rendTex.draw(vertices, 4, sf::Quads, state);
	rendTex.display();
	//tex = sf::Texture(rendTex.getTexture());
	//tex = rendTex.getTexture();
	
	state.shader = &cut;
	makeVertices(vertices, PLAYER_DRAW_SIZE);
	plTex.draw(vertices, 4, sf::Quads, state);
	cut.setParameter("tex", rendTex.getTexture());
	plTex.display();
	//playerTex = sf::Texture(plTex.getTexture());
	playerTex = plTex.getTexture();
	
	float min = 1.0f, max = 0.0f, height;
	sf::Image tmp = plTex.getTexture().copyToImage();
	//srand(int(proj->pos.x + proj->pos.y));
	sf::Vector2i point;
	for (int i = 0; i < 2500; i ++) {
			point.x = random(PLAYER_DRAW_SIZE.x, 0);
			point.y = random(PLAYER_DRAW_SIZE.y, 0);
			height = colorToFloat(tmp.getPixel(point.x, point.y).r, tmp.getPixel(point.x, point.y).g,
								  tmp.getPixel(point.x, point.y).b, tmp.getPixel(point.x, point.y).a);
			if (height > max)
				max = height;
			else if (height < min)
				min = height;
	}
	//std::cout<<min<<" "<<max<<std::endl;
	state.shader = &normalization;
	normalization.setParameter("tex", rendTex.getTexture()); // work
	normalization.setParameter("maxH", max);
	normalization.setParameter("minH", min);
	normalization.setParameter("level", proj->level);
	normalization.setParameter("scale", proj->scale);
	normalization.setParameter("cornerPos", cornerPos);
	makeVertices(vertices, SCREEN_SIZE);
	finalTex.draw(vertices, 4, sf::Quads, state);
	finalTex.display();
	
	//tex = sf::Texture(rendTex.getTexture());
	tex = finalTex.getTexture();
}
void Terrace::Draw(sf::RenderWindow &wnd, Projector * proj)
{
	sf::Vector2f texCoords[4];
	texCoords[0].x = 0.0f;
	texCoords[0].y = 0.0f;
	texCoords[1].x = SCREEN_SIZE.x;
	texCoords[1].y = 0.0f;
	texCoords[2].x = SCREEN_SIZE.x;
	texCoords[2].y = SCREEN_SIZE.y;
	texCoords[3].x = 0.0f;
	texCoords[3].y = SCREEN_SIZE.y;
	proj->Render(wnd, tex, texCoords, SCREEN_SIZE / 2.0f, SCREEN_SIZE);
}
float Terrace::GetDepth()
{
	return 1e10f;
}