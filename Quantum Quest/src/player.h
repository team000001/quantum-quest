#ifndef Quantum_Quest_player_h
#define Quantum_Quest_player_h

#include "general.h"
#include "object.h"
#include "projector.h"
#include "terrace.h"

class Player: public Object {
public:
	Player(Terrace *terr, float mass, float planc);
	~Player();
	virtual void Update(float dt, Projector *proj);
	virtual float GetDepth();
	virtual void Draw(sf::RenderWindow &wnd, Projector *proj);
	void initState(sf::Vector2f pos, float radius, float impulse);
	sf::Vector2f force;
	bool impulses;
	bool potentialP;
	bool kineticP;
	void Fourier();
	void FourierInv();
private:
	void fft(sf::RenderTexture *oldRe,
			sf::RenderTexture *oldIm,
			sf::RenderTexture *nextIm,
			sf::RenderTexture *nextRe);
	void Impulse(float dt, float scale);
	void Potential(float dt, Projector *proj);
	void getScreen(sf::Vertex vertices[4], sf::Vector2f size);
	float summate(sf::Shader *prepare, sf::Shader *summatorx, sf::Shader *summatory);
	float compress;
	sf::RenderTexture *oldRe;
	sf::RenderTexture *nextRe;
	sf::RenderTexture *oldIm;
	sf::RenderTexture *nextIm;
	sf::RenderTexture *old;
	sf::RenderTexture *next;
	sf::RenderTexture *drawOldIm;
	sf::RenderTexture *drawOldRe;
	sf::RenderTexture *drawNextIm;
	sf::RenderTexture *drawNextRe;
	sf::RenderTexture drawTexture;
	Terrace *terr;
	sf::Shader initShaderRe;
	sf::Shader initShaderIm;
	sf::Shader fftxr;
	sf::Shader fftxi;
	sf::Shader fftyr;
	sf::Shader fftyi;
	sf::Shader inverse;
	sf::Shader kinShr;
	sf::Shader kinShi;
	sf::Shader potShr;
	sf::Shader potShi;
	sf::Shader drawShader;
	sf::Shader summx;
	sf::Shader summy;
	sf::Shader maxShx;
	sf::Shader maxShy;
	sf::Shader prepare0;
	sf::Shader prepare1;
	sf::Shader prepare2;
	sf::Shader prepare3;
	sf::Shader prepare4;
	float mass;
	float planc;
	float normalization;
	sf::Transform sinceLast;
	sf::Vector2f oldPos;
	float oldDiff;
};

#endif
