 //
//  obstacles.cpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 19.05.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#include "obstacles.h"
#include <iostream>

float min (float a, float b)
{
	return a < b ? a : b;
}

inline int random (int max, int min)
{
	return min + int((max - min) * float(rand()) / RAND_MAX);
}

inline bool onScreen (sf::Vector2f vec, Projector *proj, float size) {
	return vec.x >= proj->pos.x - (SCREEN_SIZE.x * PROJECTOR_POSITION.x) / proj->scale - size
	&& vec.y >= proj->pos.y - (SCREEN_SIZE.y * PROJECTOR_POSITION.y) / proj->scale - size
	&& vec.x <= proj->pos.x + (SCREEN_SIZE.x * (1 - PROJECTOR_POSITION.x)) / proj->scale + size
	&& vec.y <= proj->pos.y + (SCREEN_SIZE.y * (1 - PROJECTOR_POSITION.y)) / proj->scale + size;
}


Obstacles::Obstacles() {
	texNames.push_back("stump.gif");
	texNames.push_back("icon.png");
}

Obstacles::~Obstacles() {
	for(int arrIndex = 0; arrIndex < int(arr.size()); arrIndex++) {
		delete arr[arrIndex];
	}
}

void Obstacles::Update(float dt, Projector *proj) {
	if (proj->pos == proj->prevPos && proj->scale == proj->prevScale)
		return;
	int obsNum = 0;
	for(int arrIndex = 0; arrIndex < int(arr.size()); arrIndex++)
		if (onScreen(*(absPosArr[arrIndex]), proj, arr[arrIndex]->size.x))
			obsNum ++;
	if (obsNum < OBSTACLES_NUM && arr.size() <= OBSTACLES_MAX_NUM
		&& rand() % int(1 / OBSTACLES_PROBABILITY) == 1) {
		//absolute size
		float obstSize = random(min(SCREEN_SIZE.x, SCREEN_SIZE.y), OBSTACLES_PONDEROUSNESS) *
							float(rand()) / RAND_MAX / proj->scale;
		// relative
		float x = SCREEN_SIZE.x * float(rand()) / RAND_MAX;
		float y = SCREEN_SIZE.y * float(rand()) / RAND_MAX;
		bool noAround = true;
		for(int arrIndex = 0; arrIndex < int(arr.size()); arrIndex++)
			if (onScreen(*(absPosArr[arrIndex]), proj, arr[arrIndex]->size.x)) {
				if (fabs(x - arr[arrIndex]->pos.x) < obstSize * proj->scale
					|| fabs(y - arr[arrIndex]->pos.y) < obstSize * proj->scale)
					noAround = false;
					break;
			}
		if (noAround) {
			arr.push_back(new Static(texNames[random(texNames.size(), 0)],
									 sf::Vector2f(x, y),
									 sf::Vector2f(obstSize, obstSize)));
			// absolute coordinates
			x = proj->pos.x + (x - SCREEN_SIZE.x * PROJECTOR_POSITION.x) / proj->scale;
			y = proj->pos.y + (y - SCREEN_SIZE.y * PROJECTOR_POSITION.y) / proj->scale;
			absPosArr.push_back(new sf::Vector2f(x,y));
		}
	}
	if (obsNum == OBSTACLES_MAX_NUM) {
		
	}
	/*
	for(int arrIndex = 0; arrIndex < int(arr.size()); arrIndex++) {
		delete arr[arrIndex];
	}
	arr.clear();
	float size = min(SCREEN_SIZE.x, SCREEN_SIZE.y);
	float obstSize;
	sf::Vector2f begin;
	sf::Vector2i obsNum;
	// for the first step
	sf::Vector2i prevNum = sf::Vector2i(WORLD_SIZE.x / size , WORLD_SIZE.y / size );
	float x, y;
	bool notDestr;
	while (size * proj->scale / 2 > OBSTACLES_PONDEROUSNESS) {
		size /= 2;
		begin.x = proj->pos.x - PROJECTOR_POSITION.x * SCREEN_SIZE.x / proj->scale;
		begin.y = proj->pos.y - PROJECTOR_POSITION.y * SCREEN_SIZE.y / proj->scale;
		int i, j;
		for (i = 0; i < SCREEN_SIZE.x / proj->scale / size ; i = i + 2)
			for (j = 0; j < SCREEN_SIZE.y / proj->scale / size ; j = j + 2) {
				obsNum = prevNum + sf::Vector2i(begin.x / size, begin.y / size) -
						sf::Vector2i(int(begin.x / size) % 2, int(begin.y / size) % 2)
						+ sf::Vector2i(i + 1, j + 1);
				srand((obsNum.x + 13234) * (obsNum.y + 23234) * 235544 - 53425);
				if (rand() % int(1 / OBSTACLES_PROBABILITY) == 1) {
					//std::cout<<"me! "<<obsNum.x<<" "<<obsNum.y<<std::endl;
					notDestr = true;
					for(int destIndex = 0; destIndex < int(destroyed.size()); destIndex++)
						if (destroyed[destIndex] == obsNum) {
							notDestr = false;
							break;
						}
					if (notDestr) {
						obstSize = size * proj->scale / 2 + random(int(size * proj->scale / 2), 0);
						if (obstSize > OBSTACLES_PONDEROUSNESS) {
							// absolute coordinates
							x = begin.x + (i + 1) * size;
							y = begin.y + (j + 1) * size;
							// relative
							x = (x - proj->pos.x) * proj->scale + PROJECTOR_POSITION.x * SCREEN_SIZE.x;
							y = (y - proj->pos.y) * proj->scale + PROJECTOR_POSITION.y * SCREEN_SIZE.y;
							
							arr.push_back(new Static(texNames[random(texNames.size(), 0)],
													 sf::Vector2f(x, y),
													 sf::Vector2f(obstSize, obstSize)));
						}
					}
				}
			}
		// half gets from the previous step
		prevNum += sf::Vector2i(WORLD_SIZE.x / size / 2, WORLD_SIZE.y / size / 2);
	}
	//std::cout<<"put! "<<prevNum.x<<std::endl;
	*/
}

void Obstacles::Draw(sf::RenderWindow &wnd, Projector *proj) {
	//std::cout<<std::endl;
	for(int arrIndex = 0; arrIndex < int(arr.size()); arrIndex++)
		if (onScreen(*(absPosArr[arrIndex]), proj, arr[arrIndex]->size.x)) {
			arr[arrIndex]->size.x = arr[arrIndex]->size.y = arr[arrIndex]->size.x * proj->scale;
			if (arr[arrIndex]->size.x > OBSTACLES_PONDEROUSNESS) {
				float x = (absPosArr[arrIndex]->x - proj->pos.x) * proj->scale
							+ SCREEN_SIZE.x * PROJECTOR_POSITION.x;
				float y = (absPosArr[arrIndex]->y - proj->pos.y) * proj->scale
							+ SCREEN_SIZE.y * PROJECTOR_POSITION.y;
				arr[arrIndex]->pos = sf::Vector2f(x, y);
				arr[arrIndex]->Draw(wnd, proj);
			}
			arr[arrIndex]->size.x = arr[arrIndex]->size.y = arr[arrIndex]->size.x / proj->scale;
		}
}

float Obstacles::GetDepth() {
	return 0.0f;
}
