//
//  obstacle.hpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_static_h
#define Quantum_Quest_static_h

#include "general.h"
#include "object.h"
#include "projector.h"

class Static : public Object {
public:
	Static(std::string texName, sf::Vector2f pos, sf::Vector2f size);
	~Static();
	virtual void Update(float dt, Projector *proj);
	virtual float GetDepth();
	virtual void Draw(sf::RenderWindow &wnd, Projector *proj);
	sf::Texture tex;
	sf::Vector2f pos, size;
};

#endif
