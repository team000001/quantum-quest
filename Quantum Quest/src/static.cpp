//
//	static.cpp
//	Quantum Quest
//
//	Created by Денис Дёмин on 29.04.14.
//	Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#include "static.h"

Static::Static(std::string texName, sf::Vector2f pos, sf::Vector2f size)
{
	if (!tex.loadFromFile(resourcePath() + texName)) {
		error(0, texName);
	}
	this->pos = pos;
	this->size = size;
}
Static::~Static(){}

void Static::Draw(sf::RenderWindow &wnd, Projector * proj)
{
	sf::Vector2f texCoords[4];
	texCoords[0].x = 0.0f;
	texCoords[0].y = 0.0f;
	texCoords[1].x = float(tex.getSize().x);
	texCoords[1].y = 0.0f;
	texCoords[2].x = float(tex.getSize().x);
	texCoords[2].y = float(tex.getSize().y);
	texCoords[3].x = 0.0f;
	texCoords[3].y = float(tex.getSize().y);
	proj->Render(wnd, tex, texCoords, pos, size);
}
void Static::Update(float dt, Projector *proj){}
float Static::GetDepth()
{
	return 0;
}
