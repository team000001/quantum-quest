//
//  constants.hpp
//  Quantum Quest
//
//  Created by Денис Дёмин on 29.04.14.
//  Copyright (c) 2014 Prihodko Demin 2111. All rights reserved.
//

#ifndef Quantum_Quest_constants_h
#define Quantum_Quest_constants_h

#define GAME_TITLE "Quantum quest"
#define SCREEN_SIZE sf::Vector2f(1200, 800) //1200x800
#define PLAYER_SIZE sf::Vector2f(256, 256)
#define PLAYER_DRAW_SIZE sf::Vector2f(512, 512)
#define PLAYER_POWER 8
#define WORLD_SIZE sf::Vector2f(1600, 1600)
#define TERRACE_PLAYFULLNESS 0.5f // less than one
#define TERRACE_PRECISION 10.0f
#define TERRACE_MAX_SIZE 0.8f //part of screen
#define PROJECTOR_POSITION sf::Vector2f(0.5f, 0.5f)
#define OBSTACLES_NUM 4
#define OBSTACLES_MAX_NUM 30
#define OBSTACLES_PONDEROUSNESS 30.0f
#define OBSTACLES_PROBABILITY 0.5f

#endif
